import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import Layout from './Layout';
import './assets/css/index.css';
import './assets/libs/fontAwesome/css/all.min.css';
import { UserProvider } from './context/AuthContext';
import { BannerTextProvider } from './context/BannerTextContext';

ReactDOM.createRoot(document.getElementById('root')).render(
    <BrowserRouter>
        <BannerTextProvider>
            <UserProvider>
                <Layout />
            </UserProvider>
        </BannerTextProvider>
    </BrowserRouter>
)
