import { ulid } from "ulid";

class Utils {
  static generateUniqueIDDocument() {
    return ulid();
  }
  static convertDate(date) {
    return (
      date &&
      `${date.toDate().toLocaleDateString("fr-FR")} à ${date
        .toDate()
        .toLocaleTimeString("fr-FR")}`
    );
  }
  static orderByName(el) {
    return el.sort((a, b) => a.name.localeCompare(b.name))
  }
  static orderByPosition(el) {
    return el.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));
  }
  static orderByPositionObject(el) {
    let orderedList;

    el.map(group => {
      // Convertir l'objet en un tableau d'entrées [clé, valeur]
      const sortedEntries = Object.entries(group).sort(([, a], [, b]) => a.position - b.position);
      // Reconstruire un nouvel objet trié
      return orderedList = Object.fromEntries(sortedEntries);
    })

    return orderedList;
  }
  static orderByBool(el) {
    if (!el) return;
    return el.sort((a, b) => {
      if (a.checked && !b.checked) {
        return 1;
      } else if (!a.checked && b.checked) {
        return -1;
      } else {
        return 0;
      }
    });
  }
  static sum(el) {
    let sum = 0;
    for (let key in el) {
      sum += parseFloat(el[key]);
    }
    return sum;
  }
  static convertNumberToCurrency(price) {
    const converter = new Intl.NumberFormat("fr-FR", {
      style: "currency",
      currency: "EUR",
    });
    return converter.format(parseFloat(price));
  }
  static removeDuplicateObjectsInArray(array) {
    const seen = new Set();
    return array.filter((item) => {
      const serializedItem = JSON.stringify(item);
      return seen.has(serializedItem) ? false : seen.add(serializedItem);
    });
  }
  static stringContainsHalal(text) {
    if (!text) return false;

    return text.toLowerCase().includes("halal");
  }
  static capitalizeFirstLetter(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }
  static calculateTotalPaginationInTable(total, page){
    return Math.ceil(total / page);
  }
}

export default Utils;
