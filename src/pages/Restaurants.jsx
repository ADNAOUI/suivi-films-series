import { useEffect, useState } from "react";
import { Accordion, Button, Tab, Tabs } from "react-bootstrap";
import ImageGallery from "react-image-gallery";
import Halal from "../assets/img/svg/halal.svg";
import { Add, CustomChekbox, Loader } from "../components";
import { useBannerText } from "../context/BannerTextContext";
import {
  addRestaurant,
  filterRestaurants,
  getRestaurants,
  getTypeRestaurants,
  getVisitedCities,
} from "../service/api";
import Utils from "../utils/Utils";

const Restaurants = () => {
  const [restaurants, setRestaurants] = useState([]);
  const [cities, setCities] = useState([]);
  const [mergeEntities, setMergeEntities] = useState([])
  const [loading, setLoading] = useState(true);
  const [typeRestaurants, setTypeRestaurants] = useState([]);
  const { setBannerText } = useBannerText();
  const [checkedItems, setCheckedItems] = useState({});
  const [active, setActive] = useState(window.innerWidth > 992 ? true : false)

  const handleChange = (event) => {
    const dataset = event.target.dataset.category
    const { name, checked } = event.target;
    setCheckedItems((prevItems) => ({
      ...prevItems,
      [name]: {
        checked,
        dataset
      },
    }));
  };

  const form = {
    name: {
      position: 1,
      trans: "Nom du restaurant",
      type: "text",
      md: 12,
      required: true,
    },
    noteFarid: {
      position: 2,
      trans: "Note Farid",
      type: "number",
      md: 6,
      required: true,
    },
    noteLaurine: {
      position: 3,
      trans: "Note Laurine",
      type: "number",
      md: 6,
      required: true,
    },
    city: {
      position: 3,
      trans: "Ville",
      type: "select",
      md: 4,
      required: true,
      options: cities,
    },
    type_restaurants: {
      position: 3,
      trans: "Type de restaurant",
      type: "select",
      md: 8,
      required: true,
      options: typeRestaurants,
    },
    review: {
      position: 3,
      trans: "Critiques",
      as: "textarea",
      rows: 3,
      md: 12,
      required: false,
    },
  };

  useEffect(() => {
    const fetchDatas = async () => {
      const restaurantsData = await getRestaurants();
      const citiesData = await getVisitedCities();
      const typeRestaurantsData = await getTypeRestaurants();

      setRestaurants(restaurantsData.datas);
      setCities(citiesData);
      setTypeRestaurants(typeRestaurantsData);
      setMergeEntities([...citiesData, ...typeRestaurantsData])
      setLoading(false);
    };

    fetchDatas();
    setBannerText(location.pathname.replace(/^\/+/g, ""));
  }, [setBannerText, active]);

  const handleRestaurantSubmit = async (data) => await addRestaurant(data);

  const filter = async (e) => {
    e.preventDefault()

    const result = {};

    for (const key in checkedItems) {
      if (checkedItems[key].checked) {
        const dataset = checkedItems[key].dataset;
        if (!result[dataset]) {
          result[dataset] = [];
        }
        result[dataset].push(key);
      }
    }

    const outputArray = result;

    const final = await filterRestaurants(outputArray);
    setRestaurants(final)
  }

  const categories = Object.entries(mergeEntities).reduce((acc, [key, value]) => {
    if (!acc[value.trans]) {
      acc[value.trans] = [];
    }
    acc[value.trans].push({ key, ...value });
    return acc;
  }, {});

  const activate = () => {
    setActive(value => !value)
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      <section className="row">
        <div className="col-12">
          <Add accept="image/*" form={form} images={true} onSubmit={handleRestaurantSubmit} />
        </div>
      </section>
      <section className="row restaurants">
        <div className="col-12 col-lg-3 ">
          <form onSubmit={filter} className={`restaurants__filter ${active ? "active" : "not-active"}`}>
            <h3 className="mb-3 fw-bolder primary-color" onClick={activate}><i className="fa-solid fa-filter"></i> Filtres</h3>
            {Object.entries(categories).map(([type, items]) => (
              <div key={type} className="restaurants__filter__item">
                <h4 className="mb-2 text-capitalize fw-bolder primary-color restaurants__filter__item__title">{type}</h4>
                <section>
                  {items.map((item, index) => {
                    return (
                      <CustomChekbox key={index} props={item} fn={handleChange} />
                    )
                  })}
                </section>
              </div>
            ))}
            <Button type="submit" variant="primary" className="background-tertiary-color">Filtrer</Button>
          </form>
        </div>

        <div className="col-12 col-lg-9 min-vh-50 restaurants__list">
          <Tabs
            defaultActiveKey={cities[0].name}
            id="justify-tab-example"
            className="mb-3"
            justify
          >
            {cities.map((item) => {
              return (
                <Tab key={item.id} eventKey={item.name} title={item.name}>
                  <Accordion>
                    {restaurants.map((restaurant, index) => {
                      if (restaurant.city == item.id) {
                        const halal = Utils.stringContainsHalal(restaurant.review)
                        return (
                          <Accordion.Item key={index} eventKey={index}>
                            <Accordion.Header>
                              <div className="restaurants__list__title">
                                {restaurant.name}
                                {halal && <img className="halal" src={Halal} alt="halal" width={25} height={25} />}
                              </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              <section className="row">
                                <div className="col-12 col-lg-4">
                                  {restaurant.images && (
                                    <ImageGallery items={restaurant.images} />
                                  )}
                                </div>
                                <div className="col-12 col-lg-8">
                                  <article>
                                    <p>
                                      note Farid : {restaurant.noteFarid}
                                      <span>/10</span>
                                    </p>
                                    <p>
                                      note Laurine : {restaurant.noteLaurine}
                                      <span>/10</span>
                                    </p>
                                  </article>
                                  <article>
                                    <h4>Critiques</h4>
                                    <p>{restaurant.review}</p>
                                  </article>
                                </div>
                              </section>
                            </Accordion.Body>
                          </Accordion.Item>
                        );
                      }
                    })}
                  </Accordion>
                </Tab>
              );
            })}
          </Tabs>
        </div>
      </section>
    </>
  );
};

export default Restaurants;
