import { useEffect } from "react";
import { useBannerText } from "../context/BannerTextContext";

const Cooking = () => {

    const { setBannerText } = useBannerText()

    useEffect(() => {
        setBannerText(location.pathname.replace(/^\/+/g, ''));

    }, [setBannerText]);


    return (
        <>
            Cooking
        </>
    )
};

export default Cooking;