import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import Table from 'react-bootstrap/Table';
import { Add, ButtonIcon, CustomSelect, Loader, Search, useToast } from "../components";
import { useBannerText } from "../context/BannerTextContext";
import { addSuivi, countSuiviState, deleteSuivi, findLabelById, getCategories, getState, getSuivi, getTables, pageSizeSuivi, searchValue, updateSuivi } from "../service/api";
import Utils from "../utils/Utils";

const Home = () => {
  const [suivi, setSuivi] = useState([]);
  const [categories, setCategories] = useState([]);
  const [states, setStates] = useState([]);
  const [stats, setStats] = useState([]);
  const [tables, setTables] = useState()
  const [loading, setLoading] = useState(true);
  const [lastVisible, setLastVisible] = useState(null);
  const [firstVisible, setFirstVisible] = useState(null);
  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("title");
  const [editingId, setEditingId] = useState(null);
  const [editingData, setEditingData] = useState({});
  const [totalPagination, setTotalPagination] = useState(0);

  const { setBannerText } = useBannerText();
  const showToast = useToast();

  const form = {
    initiator: {
      position: 1,
      trans: "Initiateur",
      type: "text",
      md: 12,
      required: true,
    },
    title: {
      position: 2,
      trans: "Titre",
      type: "text",
      md: 12,
      required: true,
    },
    category: {
      position: 3,
      trans: "Catégories",
      type: "select",
      md: 6,
      required: true,
      options: categories,
    },
    state: {
      position: 4,
      trans: "Etat d'avancement",
      type: "select",
      md: 6,
      required: true,
      options: states,
    },
    noteFarid: {
      position: 5,
      trans: "Note Farid",
      type: "number",
      md: 4,
      required: true,
    },
    noteLaurine: {
      position: 6,
      trans: "Note Laurine",
      type: "number",
      md: 4,
      required: true,
    },
  };

  useEffect(() => {
    const fetchDatas = async () => {
      const { data, firstVisibleDocument, lastVisibleDocument } = await getSuivi();
      const categoryData = await getCategories();
      const stateData = await getState();
      const statData = await countSuiviState();
      const table = await getTables("home");
      const totalPaginations = Utils.calculateTotalPaginationInTable(statData[4].count, pageSizeSuivi);

      setSuivi(data);
      setCategories(categoryData);
      setStates(stateData);
      setStats(statData);
      setTables(table);
      setFirstVisible(firstVisibleDocument);
      setLastVisible(lastVisibleDocument);
      setLoading(false);
      setTotalPagination(totalPaginations);
    };
    setBannerText("films et series");
    fetchDatas();
  }, [setBannerText]);

  const loadMoreData = async (direction) => {
    setIsLoadingMore(true);
    let cursor = direction === "next" ? lastVisible : firstVisible;
    const { data, firstVisibleDocument, lastVisibleDocument } = await getSuivi(pageSizeSuivi, cursor, direction, order);

    setSuivi(data);
    setFirstVisible(firstVisibleDocument);
    setLastVisible(lastVisibleDocument);
    setIsLoadingMore(false);

    if (direction === "next") {
      setPage(prevPage => prevPage + 1);
    } else if (direction === "prev") {
      setPage(prevPage => prevPage - 1);
    }
  };

  const handleHomeSubmit = async (data) => await addSuivi(data);

  function handleEdit(id) {
    setEditingId(id);
    const currentItem = suivi.find((item) => item.id === id);
    setEditingData(currentItem);
  }

  function handleChange(field, value) {
    // Met à jour editingData avec les nouvelles valeurs
    setEditingData((prev) => ({
      ...prev,
      [field]: value,
    }));
  }

  const saveChanges = async () => {
    setEditingId(null);
    try {
      await updateSuivi(editingData);
      setEditingData({});

      setSuivi((prevSuivi) =>
        prevSuivi.map((item) =>
          item.id === editingData.id ? { ...item, ...editingData } : item
        )
      );

      showToast("Données soumis avec succès !", "success");
    } catch (error) {
      showToast("Erreur lors de la soumission", "error");
      console.error(error);
    }
  };

  function handleDelete(id) {
    if (confirm("Êtes-vous sur de supprimer cette donnée ?")) {
      deleteSuivi(id);
      showToast("Donnée supprimée avec succès !", "success");
    }
  }

  async function filter(el) {
    setOrder(el); // Mettez à jour l'état `order` pour le nouveau critère de tri
    setPage(0); // Réinitialisez la pagination à la première page
    const { data, firstVisibleDocument, lastVisibleDocument } = await getSuivi(pageSizeSuivi, null, "next", el);
    setSuivi(data);
    setFirstVisible(firstVisibleDocument);
    setLastVisible(lastVisibleDocument);
  }

  const searchDatas = async (e) => {
    const el = e.target.value;

    if (el.length > 1) {
      const search = await searchValue(el);
      setSuivi(search);
    }
    else {
      e.target.addEventListener("keydown", async function (e) {
        if (e.key === "Backspace") {
          const { data } = await getSuivi(pageSizeSuivi, null, "next");
          setSuivi(data);
        }
      });
    }
  }

  if (loading)
    return <Loader />;

  return (
    <>
      <section className="home row">
        <div className="col-12">
          <Add form={form} onSubmit={handleHomeSubmit} />
        </div>
      </section>
      <section className="row">
        <div className="col-12">
          <Search title="Rechercher un film" id="search" fnOnChange={(e) => searchDatas(e)} />
        </div>
      </section>
      <section className="row table-firebase">
        <div className="col-12">
          <Table responsive>
            <thead>
              <tr>
                {tables["head"] && Utils.orderByPosition(tables["head"]).map((item, index) => <th key={index} onClick={(e) => filter(item.label)}>{item.trans}</th>)}
              </tr>
            </thead>
            <tbody>
              {suivi.map((item, index) => (
                <tr key={index} className={`state ${findLabelById(item.state, states)}`}>
                  <td>{index + page * pageSizeSuivi}</td>
                  <td>
                    <textarea
                      type="text"
                      value={editingId === item.id ? editingData.initiator : item.initiator}
                      readOnly={editingId !== item.id}
                      onChange={(e) =>
                        handleChange("initiator", e.target.value)
                      }
                    />
                  </td>
                  <td>
                    <textarea
                      className="titre"
                      value={editingId === item.id ? editingData.title : item.title}
                      readOnly={editingId !== item.id}
                      onChange={(e) => handleChange("title", e.target.value)}
                    />
                  </td>
                  <td>
                    <CustomSelect
                      options={categories}
                      value={editingId === item.id ? editingData.category : item.category}
                      onChange={(e) =>
                        handleChange("category", e.target.value)
                      }
                    />
                  </td>
                  <td>
                    <CustomSelect
                      options={states}
                      value={editingId === item.id ? editingData.state : item.state}
                      onChange={(e) =>
                        handleChange("state", e.target.value)
                      }
                    />
                  </td>
                  <td>
                    <input
                      type="number"
                      value={editingId === item.id ? editingData.noteFarid != 0 ? editingData.noteFarid : "" : item.noteFarid != 0 ? item.noteFarid : ""}
                      readOnly={editingId !== item.id}
                      onChange={(e) =>
                        handleChange("noteFarid", e.target.value)
                      }
                    />
                  </td>
                  <td>
                    <input
                      type="number"
                      value={editingId === item.id ? editingData.noteLaurine != 0 ? editingData.noteLaurine : "" : item.noteLaurine != 0 ? item.noteLaurine : ""}
                      readOnly={editingId !== item.id}
                      onChange={(e) =>
                        handleChange("noteLaurine", e.target.value)
                      }
                    />
                  </td>
                  <td>
                    {editingId === item.id ? (
                      <ButtonIcon
                        variant="success"
                        title="sauvegarder"
                        icon={<i className="fa-solid fa-save"></i>}
                        fnUpdate={() => saveChanges(item.id)}
                      />
                    ) : (
                      <ButtonIcon
                        title="modifier"
                        icon={<i className="fa-solid fa-pen-to-square"></i>}
                        fnUpdate={() => handleEdit(item.id)}
                      />
                    )}
                  </td>
                  <td>
                    <ButtonIcon
                      variant="danger"
                      title="supprimer"
                      icon={<i className="fa-solid fa-trash"></i>}
                      fnUpdate={() => handleDelete(item.id)}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <div className="col-12">
          <div className="mb-5 table-firebase-btn">
            <Button className="prev background-tertiary-color" onClick={() => loadMoreData("prev")} disabled={isLoadingMore || page === 0}>
              Précédent
            </Button>
            <Button className="next background-tertiary-color" onClick={() => loadMoreData("next")} disabled={isLoadingMore || page >= totalPagination -1}>
              Suivant
            </Button>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
