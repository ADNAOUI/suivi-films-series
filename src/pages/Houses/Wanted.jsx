import { useEffect, useState } from "react";
import { Add, Title } from "../../components";
import { useUser } from "../../context/AuthContext";
import { useBannerText } from "../../context/BannerTextContext";
import { addHousesElementsByDoc, getHousesElementsByDoc, update } from "../../service/api";

const docId = "Wanted"

const Wanted = () => {
    const [todos, setTodos] = useState([]);
    const { setBannerText } = useBannerText();
    const { user } = useUser();

    const form = {
        name: {
            position: 1,
            trans: "Label",
            type: "text",
            md: 12,
            required: true,
            category: "non alimentaires",
        },
        url: {
            position: 2,
            trans: "Lien",
            type: "text",
            md: 12,
            required: true,
            category: "non alimentaires"
        },
        reason: {
            position: 3,
            trans: "Raison",
            type: "text",
            md: 12,
            required: true,
            category: "non alimentaires"
        }
    };

    const fetchDatas = async () => {
        let el = await getHousesElementsByDoc(docId);
        setTodos(el.datas);
    };

    useEffect(() => {
        fetchDatas();
        setBannerText("maisons et appartements")

    }, [setBannerText]);

    const handleSubmit = async (data) => {
        data.email = user.email;
        await addHousesElementsByDoc(data, docId, true)
        fetchDatas()
    };

    const toggleCheck = async (data) => {
        const updatedItem = { ...data, checked: true };

        await update(docId, data, true)
        await update(docId, updatedItem)
        fetchDatas();
    }

    const wanted = () => {
        if (todos.length > 0) {
            const newArray = todos.reduce((acc, current) => {
                const email = current.email;
                if (!acc[email]) {
                    acc[email] = [];
                }
                acc[email].push(current);
                return acc;
            }, {})

            return Object.entries(newArray).map(([email, items]) => (
                <div key={email} className="col">
                    <h3 className="primary-color">{email}</h3>
                    {items.map((data, index) => (
                        <article key={index} className={`${data.checked ? "wanted-buy" : ""} mt-2 p-3`} onClick={() => toggleCheck(data)}>
                            <h4 className="m-0 secondary-color">{data.name}</h4>
                            <p className="m-0 text-content"><a href={data.url} target="_blank">{data.url}</a></p>
                            <p className="m-0">{data.reason}</p>
                        </article>
                    ))}
                </div>
            ))
        }
    }

    return (
        <section className="wanted row justify-content-center">
            <div className="col-12">
                <Title text="Liste d'envies" />
            </div>
            <div className="col-12">
                <Add form={form} onSubmit={handleSubmit} />
            </div>
            <div className="col-12">
                <section className="row rg-10">
                    {wanted()}
                </section>
            </div>
        </section>
    );
}
export default Wanted;