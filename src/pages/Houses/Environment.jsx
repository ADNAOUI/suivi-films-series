import { useEffect, useState } from "react";
import { ListGroup } from "react-bootstrap";
import { Add, Loader, Title } from "../../components";
import { useBannerText } from "../../context/BannerTextContext";
import { addHousesElementsByDoc, getHousesElementsByDoc, getTables } from "../../service/api";
import Utils from "../../utils/Utils";

const Environment = () => {
    const [total, setTotal] = useState()
    const [environment, setEnvironment] = useState()
    const [loading, setLoading] = useState(true)
    const { setBannerText } = useBannerText()
    const [tables, setTables] = useState();

    const fetchDatas = async () => {
        const env = await getHousesElementsByDoc("Environment")
        const data = env.datas[0]
        // delete data.create_at;

        const table = await getTables("Environment");
        setTotal(Utils.sum(data));
        setEnvironment(data);
        setLoading(false);
        setTables(table);
    };

    useEffect(() => {
        fetchDatas();
        setBannerText("maisons et appartements")

    }, [setBannerText]);

    const handleSubmit = async (data) => {
        await addHousesElementsByDoc(data, "Environment", false)
        fetchDatas()
    };

    if (loading) {
        return <Loader />
    } else {
        const categories = Object.entries(tables["form"][0]).reduce((acc, [key, value]) => {
            if (!acc[value.category]) {
                acc[value.category] = [];
            }
            acc[value.category].push({ key, ...value });
            return acc;
        }, {});

        return (
            <div className="row environment">
                <div className="col-12 vh-50">
                    <Title text="Rappel des dépenses" />
                    <section className="row">
                        <Add form={tables["form"][0]} onSubmit={handleSubmit} />
                    </section>
                    <section className="row row-gap-3 mb-3">
                        {Object.entries(categories).map(([category, items]) => {
                            const sousTotal = items.reduce((total, { key }) => {
                                return environment.hasOwnProperty(key) ? total + Number(environment[key]) : total;
                            }, 0);

                            return (
                                <div className="col-12 col-md-4" key={category}>
                                    <h3 className="text-center text-capitalize mb-3 primary-color fw-bolder">{category}</h3>
                                    <ListGroup>
                                        {items.map(({ key, trans }) => {
                                            if (environment.hasOwnProperty(key)) {
                                                return (
                                                    <ListGroup.Item key={key}>
                                                        {trans} - <span className="secondary-color">{Utils.convertNumberToCurrency(environment[key])}</span>
                                                    </ListGroup.Item>
                                                );
                                            }
                                            return null; // Si la clé n'existe pas dans Environment, ne rien rendre
                                        })}
                                        <ListGroup.Item className="subTotal">
                                            Sous total - <span className="secondary-color">{Utils.convertNumberToCurrency(sousTotal)}</span>
                                        </ListGroup.Item>
                                    </ListGroup>
                                </div>
                            )
                        })}
                    </section>
                    <section className="row pb-5">
                        <div className="col-12">
                            <ListGroup.Item className="total">
                                <div>
                                    <span>Total&nbsp;</span>
                                    <span className="primary-color fw-bolder">{Utils.convertNumberToCurrency(total)}</span>
                                </div>
                                <div>
                                    <span>Soit un total par personne de&nbsp;</span>
                                    <span className="primary-color fw-bolder">{Utils.convertNumberToCurrency(total / 2)}</span>
                                </div>
                            </ListGroup.Item>
                        </div>
                    </section>
                </div>
            </div>
        )
    }
};

export default Environment;