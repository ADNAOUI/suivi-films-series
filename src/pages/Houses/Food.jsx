import { useEffect, useState } from "react";
import { Button, Col, Form, ListGroup, Row } from "react-bootstrap";
import { ButtonIcon, Title } from "../../components";
import { useBannerText } from "../../context/BannerTextContext";
import { remove, getHousesElementsByDoc, update } from "../../service/api";
import Utils from "../../utils/Utils";

const docId = "Food"

const Food = () => {
    const [todos, setTodos] = useState([]);
    const [input, setInput] = useState('');
    const { setBannerText } = useBannerText()

    const fetchDatas = async () => {
        let el = await getHousesElementsByDoc(docId);
        setTodos(Utils.orderByBool(el.datas))
    };

    useEffect(() => {
        fetchDatas();
        setBannerText("maisons et appartements")

    }, [setBannerText]);

    const addTodo = async (e) => {
        e.preventDefault();
        if (input.trim() === '') return;

        await update(docId, {
            name: input,
            checked: false
        })
        setInput('');
        fetchDatas();
    };

    const removeProduct = async (product) => {
        await update(docId, product, true)
        fetchDatas();
    };

    const toggleCheck = async (product) => {
        const updatedProduct = { ...product, checked: !product.checked };
        await update(docId, product, true); // Supprime l'ancien objet
        await update(docId, updatedProduct); // Ajoute l'objet mis à jour
        fetchDatas();
    };

    const deletedFood = async () => {
        await remove(docId);
        fetchDatas();
    }

    return (
        <section className="food row justify-content-center">
            <div className="col-12">
                <Title text="Liste de courses" />
            </div>
            <div className="col-12">
                <Form onSubmit={addTodo} className="mt-3">
                    <Form.Group as={Row} className="mb-3" controlId="product">
                        <Col xs="12">
                            <div className="d-flex align-items-center gap-3">
                                <Form.Label className="m-0">
                                    Produit
                                </Form.Label>
                                <Form.Control
                                    type="text"
                                    value={input}
                                    onChange={e => setInput(e.target.value)} />
                                <Button className="background-tertiary-color" type="submit">Ajouter</Button>
                            </div>
                        </Col>
                    </Form.Group>
                </Form>
            </div>
            <div className="col-12 col-sm-8">
                <ListGroup className="food__list">
                    {todos.map((todo, index) => {
                        return (
                            <ListGroup.Item key={index} className={`food__list__item ${todo.checked ? 'checked' : ""}`}>
                                <input type="checkbox"
                                    onChange={() => toggleCheck(todo)}
                                    checked={todo.checked} />
                                <p className="food__list__item__product">{todo.name}</p>
                                <ButtonIcon
                                    classname="background-danger-color"
                                    variant="danger"
                                    title="Supprimer"
                                    icon={<i className="fa-regular fa-trash-can"></i>}
                                    fnUpdate={() => removeProduct(todo)}
                                />
                            </ListGroup.Item>
                        )
                    })}
                </ListGroup>
                <div className="mt-4">
                    {todos.length > 0 ? <Button type="button" className="background-danger-color" variant="danger" onClick={deletedFood}>Supprimer la liste</Button> : ""}
                </div>
            </div>
        </section>
    );
}
export default Food