import { useEffect, useState } from "react";
import { CustomCard, Loader } from "../../components";
import { useBannerText } from "../../context/BannerTextContext";
import { getLink } from "../../service/api";


const Houses = () => {
  const [loading, setLoading] = useState(true);
  const { setBannerText } = useBannerText();
  const [links, setLinks] = useState([])

  useEffect(() => {
    const fetchDatas = async () => {
      const linksData = await getLink();

      setLinks(linksData.datas)
      setLoading(false);
    };

    fetchDatas();
    setBannerText("maisons et appartements");
  }, [setBannerText]);

  if (loading) return <Loader />;

  return (
    <section className="row">
      {links &&
        links.map((item, index) => {
          return (
            <div className="col-6 col-md-3 mb-3" key={index}>
              <CustomCard card={item} />
            </div>
          )
        })}
    </section>
  );
};

export default Houses;
