import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { Add, Loader } from "../../components";
import { useBannerText } from "../../context/BannerTextContext";
import { addHousesElementsByDoc, getHousesElementsByDoc } from "../../service/api";
import Utils from "../../utils/Utils";

const head = [
    {
        label: "n°",
    },
    {
        label: "Adresse",
    },
    {
        label: "Type",
    },
    {
        label: "Date de visite",
    },
    {
        label: "Avis",
    },
    {
        label: "Infos",
    },
];

const Search = () => {
    const [houses, setHouses] = useState([]);
    const [loading, setLoading] = useState(true);
    const { setBannerText } = useBannerText()

    const form = {
        address: {
            position: 1,
            trans: "Adresse du bien",
            type: "text",
            md: 12,
            required: true,
        },
        type: {
            position: 2,
            trans: "Maison ou Appartement",
            type: "text",
            md: 6,
            required: true,
        },
        date_visite: {
            position: 3,
            trans: "Date de visite",
            type: "date",
            md: 6,
            required: true,
        },
        avis: {
            position: 3,
            trans: "Avis",
            type: "radio",
            radioInput: [
                {
                    yes: {
                        trans: "oui",
                    },
                },
                {
                    no: {
                        trans: "non",
                    },
                },
            ],
            // radioInput: ["oui", "non"],
            md: 4,
            required: true,
        },
        info: {
            position: 3,
            trans: "Informations complémentaires",
            type: "text",
            md: 8,
            required: true,
        },
    };

    useEffect(() => {
        const fetchDatas = async () => {
            const HousesData = await getHousesElementsByDoc("Search");

            setHouses(HousesData);
            setLoading(false);
        };

        fetchDatas();
        setBannerText("maisons et appartements")

    }, [setBannerText]);

    const handleHousesSubmit = async (data) => await addHousesElementsByDoc(data, "Search");

    if (loading) return <Loader />;

    return (
        <div className="row">
            <div className="col-12">
                <Add form={form} onSubmit={handleHousesSubmit} />
            </div>

            <div className="col-12 vh-50">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            {head.map((item, index) => {
                                return <th key={index}>{item.label}</th>;
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {houses &&
                            houses.datas.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index}</td>
                                        <td>{item.address}</td>
                                        <td>{item.type}</td>
                                        <td>{Utils.convertDate(item.date_visite)}</td>
                                        <td>{item.avis ? "oui" : "non"}</td>
                                        <td>{item.info}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </Table>
            </div>
        </div>
    )
};

export default Search;