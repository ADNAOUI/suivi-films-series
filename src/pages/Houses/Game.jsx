import { useEffect, useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { Add, Title } from "../../components";
import { useUser } from "../../context/AuthContext";
import { useBannerText } from "../../context/BannerTextContext";
import { addHousesElementsByDoc, getHousesElementsByDoc, getTables } from "../../service/api";
import Utils from "../../utils/Utils";

const docId = "Game"

const Game = () => {
    const [todos, setTodos] = useState([]);
    const [tables, setTables] = useState();
    const [loading, setLoading] = useState(false)
    const { setBannerText } = useBannerText();
    const { user } = useUser();

    const fetchDatas = async () => {
        let el = await getHousesElementsByDoc(docId);
        el = Utils.orderByName(el.datas);
        setTodos(el);

        let table = await getTables(docId);
        table = Utils.orderByPositionObject(table["form"]);
        setTables(table);
        
        setLoading(true);
    };

    useEffect(() => {
        fetchDatas();
        setBannerText("maisons et appartements")

    }, [setBannerText]);

    const handleSubmit = async (data) => {
        data.email = user.email;
        await addHousesElementsByDoc(data, docId, true)
        fetchDatas()
    };

    const wanted = () => {
        if (todos.length > 0) {
            const newArray = todos.reduce((acc, current) => {
                const category = current.category;
                if (!acc[category]) {
                    acc[category] = [];
                }
                acc[category].push(current);
                return acc;
            }, {});


            return (
                <Tabs
                    // defaultActiveKey={cities[0].name}
                    id="justify-tab-example"
                    className="mb-3"
                    justify
                >
                    {Object.entries(newArray).map(([category, items]) => {
                        return (
                            < Tab
                                key={category}
                                eventKey={category}
                                title={category}
                                className="games__wrapper"
                            >
                                {items.map((data, index) => (
                                    <article key={index} className="games__wrapper__article mt-2">
                                        <h4 className="m-0 fst-italic secondary-color">{data.name}</h4>
                                        <ul className="ps-3">
                                            <li><p className="m-0">Note Farid : {data.noteFarid}/10</p></li>
                                            <li><p>Note Laurine : {data.noteLaurine}/10</p></li>
                                        </ul>
                                    </article>
                                ))}
                            </Tab>
                        )
                    })}
                </Tabs >
            );
        }
    }

    if (loading)
        return (
            <section className="games row justify-content-center">
                <div className="col-12">
                    <Title text="Jeux à la maison" />
                </div>
                <div className="col-12">
                    <Add form={tables} onSubmit={handleSubmit} />
                </div>
                <div className="col-12">
                    <section>
                        {wanted()}
                    </section>
                </div>
            </section>
        );
}
export default Game;