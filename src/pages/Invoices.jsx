import { Fragment, useEffect, useState } from "react";
import { Accordion, Col, Row } from "react-bootstrap";
import { Add, Loader } from "../components";
import { useBannerText } from "../context/BannerTextContext";
import { addInvoice, getInvoices } from "../service/api";

const Invoices = () => {
    const [invoices, setInvoices] = useState([]);
    const [loading, setLoading] = useState(true);
    const { setBannerText } = useBannerText()

    const form = {
        name: {
            position: 1,
            trans: "Nom du magasin",
            type: "text",
            md: 12,
            required: true,
        },
        info: {
            position: 2,
            trans: "Facture",
            type: "text",
            md: 12,
            required: true,
        }
    };

    const fetchDatas = async () => {
        const invoicesData = await getInvoices();

        setInvoices(invoicesData);
        setLoading(false);
    };

    useEffect(() => {
        setBannerText(location.pathname.replace(/^\/+/g, ''));

        fetchDatas();
    }, [setBannerText]);

    const handleSubmit = async (data) => {
        await addInvoice(data)
        fetchDatas();

    };

    if (loading)
        return (<Loader />)

    return (
        <>
            <Add
                accept="application/pdf"
                form={form}
                files={true}
                onSubmit={handleSubmit}
            />

            <Row className="mb-3">
                {invoices.map((invoice, index) => {
                    return (
                        <Col xs={12} lg={4} key={index}>
                            <Accordion>
                                <Accordion.Item key={index} eventKey={index}>
                                    <Accordion.Header>{invoice.id}</Accordion.Header>
                                    <Accordion.Body>
                                        {invoice.data.map((item, index) => {
                                            return (
                                                <section className="row invoice-bloc" key={index}>
                                                    {item.files && (item.files).map((file, key) => {
                                                        return (
                                                            <Fragment key={key}>
                                                                <div className="col-7">
                                                                    <span>{item.info}</span>
                                                                </div>
                                                                <div className="col-5">
                                                                    <a className="link" href={file.original} target="_blank" download="file">Voir la facture</a>
                                                                </div>
                                                            </Fragment>
                                                        )
                                                    })}
                                                </section>
                                            )
                                        })}
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </Col>
                    );
                })}
            </Row>
        </>
    )
};

export default Invoices;