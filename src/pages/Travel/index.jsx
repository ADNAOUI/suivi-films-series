import { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import { CustomCard } from "../../components";
import { useBannerText } from "../../context/BannerTextContext";
import { getTravels } from "../../service/api/travels";

const Travel = () => {
  const [travels, setTravels] = useState("");
  const [travelImages, setTravelImages] = useState("");
  const { setBannerText } = useBannerText();
  const location = useLocation();

  useEffect(() => {
    const fetchDatas = async () => {
      const travelsData = await getTravels();

      // travelsData.map(item => {
      //     item.image = "https://c4.wallpaperflare.com/wallpaper/377/82/449/5bf55b183fa85-wallpaper-preview.jpg"
      //     item.link = `${location.pathname}/${item.id}`
      // })

      setTravels(travelsData);
    };

    setBannerText(location.pathname.replace(/^\/+/g, ""));
    fetchDatas();
  }, [setBannerText]);

  return (
    <Row xs={1} md={2} className="g-4">
      {travels &&
        travels.map((item, index) => (
          <Col key={index}>
            <CustomCard card={item} />
          </Col>
        ))}
    </Row>
  );
};

export default Travel;
