import { useEffect } from "react";
import { useParams } from 'react-router-dom';
import { useBannerText } from "../../context/BannerTextContext";

const City = () => {

    const { idCity } = useParams()
    const { setBannerText } = useBannerText()

    useEffect(() => {
        setBannerText("Italie")
    }, [setBannerText]);

    useEffect(() => {
        console.log(idCity)
    }, [])


    return (
        <>
            city
        </>
    )
};

export default City;