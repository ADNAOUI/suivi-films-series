import { useEffect } from "react";
import { NavLink, useLocation, useParams } from 'react-router-dom';
import { useBannerText } from "../../context/BannerTextContext";

const Country = () => {

    const { idCountry } = useParams()
    const { setBannerText } = useBannerText()
    const nav = useLocation()

    useEffect(() => {
        setBannerText("Italie")
    }, [setBannerText]);

    useEffect(() => {
        console.log(nav.pathname)
    }, [])


    return (
        <>
            <li>
                <NavLink to={`${nav.pathname}/5`}>Home</NavLink>
            </li>
        </>
    )
};

export default Country;