import { collection, getDocs } from "firebase/firestore/lite";

import { db } from "../firebase/config";

const name = "Travels";

export const getTravels = async () => {
    try {
        const querySnapshot = await getDocs(collection(db, name))
        let datas = [];

        querySnapshot.forEach((doc) => {
            datas.push({
                ...doc.data(),
                idTravel: doc.id
            });
        })

        return datas;
    } catch (error) {
        console.log(error);
        return [];
    }
}

export const getVisitedCities = async () => {
try {
        const cities = await getDocs(collection(db, "Cities"))
        let datas = [];

        cities.forEach((city) => {
            datas.push({
                ...city.data(),
                id: city.id,
                type: "city", 
                trans: "Villes"
            });
        })

        return datas;
    } catch (error) {
        console.log(error);
        return [];
    }
}