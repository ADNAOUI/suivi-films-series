import { collection, getDocs } from "firebase/firestore/lite";
import { db } from "../firebase/config";

export const getCategories = async () => {
    try {
        const querySnapshot = await getDocs(collection(db, 'Categories'))
        let datas = [];

        querySnapshot.forEach((doc) => {
            datas.push(doc.data());
        })

        return datas;
    } catch (error) {
        console.log(error);
        return [];
    }
}

export const findLabelById = (id, items) => {
    const item = items.find(item => item.id === id);
    return item ? item.label : 'unknown'; // Retourne 'Non spécifié' si l'ID n'est pas trouvé
};