import {
  Timestamp,
  addDoc,
  collection,
  getDocs,
  query,
  where
} from "firebase/firestore/lite";
import { db } from "../firebase/config";

const name = "Restaurants";

export const getRestaurants = async () => {
  let datas = [];
  let loading = true;

  try {
    const restaurants = await getDocs(collection(db, name));

    restaurants.forEach((restaurant) => {
      datas.push(restaurant.data());
    });
  } catch (error) {
    console.log("error", error);
  } finally {
    loading = false;
  }

  return { datas, loading };
};

export const getTypeRestaurants = async () => {
  let datas = [];

  try {
    const types = await getDocs(collection(db, "Type_Restaurants"));

    types.forEach((type) => {
      datas.push({
        ...type.data(),
        id: type.id,
        type: "type_restaurants",
        trans: "Restaurants"
      });
    });

    return datas;
  } catch (error) {
    console.log(error);
  }
};

export const addRestaurant = async (data) => {
  data.create_at = Timestamp.now();

  try {
    await addDoc(collection(db, name), data);
  } catch (error) {
    console.log(error);
  }
};

export const filterRestaurants = async (el) => {
  const filters = [];
  const elements = [];

  Object.entries(el).forEach(([key, values]) => {
    filters.push(where(key, "in", values));
  })

  const q = query(collection(db, 'Restaurants'), ...filters);

  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    elements.push(doc.data());
  });

  return elements;
}