import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  endBefore,
  getCount,
  getDocs,
  limit,
  limitToLast,
  orderBy,
  query,
  serverTimestamp,
  startAfter,
  updateDoc,
  where
} from "firebase/firestore/lite";

import { db } from "../firebase/config";

const name = "Suivi";

export const pageSizeSuivi = 25;

export const getSuivi = async (
  pageSize = pageSizeSuivi,
  cursor = null,
  direction = "next",
  order = "title"
) => {
  try {
    const coll = collection(db, name);

    let q;

    if (direction === "next") {
      q = cursor
        ? query(coll, orderBy(order), startAfter(cursor), limit(pageSize))
        : query(coll, orderBy(order), limit(pageSize));
    } else if (direction === "prev") {
      q = cursor
        ? query(
          coll,
          orderBy(order),
          endBefore(cursor),
          limitToLast(pageSize)
        )
        : query(coll, orderBy(order), limitToLast(pageSize));
    }

    const documentSnapshots = await getDocs(q);
    const firstVisibleDocument = documentSnapshots.docs[0];
    const lastVisibleDocument =
      documentSnapshots.docs[documentSnapshots.docs.length - 1];

    const data = documentSnapshots.docs.map((doc) => ({
      ...doc.data(),
      id: doc.id,
    }));

    return { data, firstVisibleDocument, lastVisibleDocument };
  } catch (error) {
    console.log("error", error);
  }
};

export const addSuivi = async (props) => {
  props.dateCreation = serverTimestamp();

  changeTypeElements(props);

  try {
    await addDoc(collection(db, name), props);
  } catch (error) {
    console.log(error);
  }
};

export const updateSuivi = async (props) => {
  props.dateUpdate = serverTimestamp();

  changeTypeElements(props);

  try {
    await updateDoc(doc(db, name, props.id), props);
  } catch (error) {
    console.log(error);
  }
};

export const deleteSuivi = async (el) => {
  try {
    await deleteDoc(doc(db, name, el));
  } catch (error) {
    console.log(error);
  }
};

export const countSuiviState = async () => {
  let datas = [];
  try {
    const coll = collection(db, name);
    const states = await getDocs(collection(db, "States"));

    for (const state of states.docs) {
      const q = query(coll, where("state", "==", state.id));
      const snapshot = await getCount(q);

      datas.push({
        count: snapshot.data().count,
        name: state.data().label,
      });
    }

    const total = await getCount(coll);

    datas.push({
      count: total.data().count,
      name: "total",
    });

    return datas;
  } catch (error) {
    console.log("error", error);
  }
};

export const searchValue = async (el) => {
  const coll = collection(db, name);
  const q = query(coll);

  const documentSnapshots = await getDocs(q);

  const data = documentSnapshots.docs.map((doc) => ({
    ...doc.data(),
    id: doc.id,
  }));

  if (el && el.length > 2) {
    let result = [];

    data.map(item => {
      const title = item.title;

      if (title.includes(el))
        result.push(item);
    })

    return result;
  }

  return data;
}

function changeTypeElements(el) {
  if (isNaN(el.noteFarid) && isNaN(el.noteLaurine)) {
    el.noteFarid = 0;
    el.noteLaurine = 0;
  }

  el.noteFarid = Number(el.noteFarid);
  el.noteLaurine = Number(el.noteLaurine);
  el.title = el.title.toLowerCase();
}

export const filterSuivi = async (el) => {
  const coll = collection(db, name);
  let q = query(coll);

  const documentSnapshots = await getDocs(q);

  const datas = documentSnapshots.docs.map((doc) => ({
    ...doc.data(),
    id: doc.id,
  }));

  datas.map(item => {
    // updateDoc(doc(db, "Suivi", item.id), item)

    item.title = item.title.toLowerCase();
    // updateDoc(doc(db, "Suivi", item.id), item)
    //mettre les notes en Number
    // if (isNaN(item.noteFarid)) {
    //   item.noteFarid = 0
    //   item.noteLaurine = 0
    // }

    //changer le state 
    // if (item.state == "1") {
    //   item.state = "Yp3plBJpmlwAMds0M0cb"
    // }
    // if (item.state == "2") {
    //   item.state = "ghgRUESBT9nfeRnDaQPy"
    //   updateDoc(doc(db, "Suivi", item.idSuivi), item)
    // }
    // if (item.state == "3") {
    //   item.state = "jYS2eEh25QnaWY9Peg44"
    //   updateDoc(doc(db, "Suivi", item.idSuivi), item)
    // }
  })
}