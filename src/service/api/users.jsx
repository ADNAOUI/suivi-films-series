import { collection, getDocs } from "firebase/firestore/lite";
import { db } from "../firebase/config";

export const getUsers = async () => {
    try {
        const querySnapshot = await getDocs(collection(db, 'Users'))
        let datas = [];

        querySnapshot.forEach((doc) => {
            datas.push(doc.data());
        })

        return datas;
    } catch (error) {
        console.log(error);
        return [];
    }
}