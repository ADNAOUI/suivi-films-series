import { doc, getDoc, setDoc } from "firebase/firestore/lite";
import { db } from "../firebase/config";

const table = "Tables";

export const getTables = async (idDoc) => {
  try {
    const docRef = doc(db, table, idDoc);
    const querySnapshot = await getDoc(docRef)

    return querySnapshot.data();

  } catch (error) {
    console.log(error);
  }
}

export const addTables = async (docId) => {
  let data = {};

  data.form = [
    {
      name: {
        position: 1,
        trans: "Label",
        type: "text",
        md: 12,
        required: true,
      },
      noteFarid: {
        position: 2,
        trans: "Note Farid",
        type: "number",
        md: 6,
      },
      noteLaurine: {
        position: 3,
        trans: "Note Laurine",
        type: "number",
        md: 6,
      },
      category: {
        position: 3,
        trans: "Catégorie",
        type: "text",
        md: 12,
        required: true,
      }
    }
  ]

  // data.head = [
  //   {
  //     "position": "0",
  //     "label": "n°"
  //   },
  //   {
  //     "position": "1",
  //     "label": "Initiateur"
  //   },
  //   {
  //     "label": "Titre",
  //     "position": "2"
  //   },
  //   {
  //     "label": "Catégorie",
  //     "position": "3"
  //   },
  //   {
  //     "position": "4",
  //     "label": "Etat d'avancement"
  //   },
  //   {
  //     "label": "Note Farid (/10)",
  //     "position": "5"
  //   },
  //   {
  //     "position": "6",
  //     "label": "Note Laurine (/10)"
  //   },
  //   {
  //     "label": "Modifier",
  //     "position": "7"
  //   },
  //   {
  //     "label": "Supprimer",
  //     "position": "8"
  //   }
  // ]
  // delete data["body"]
  try {
    await setDoc(doc(db, table, docId), data);
  } catch (error) {
    console.log(error);
  }
};
