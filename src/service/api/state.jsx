import { collection, doc, getDocs, updateDoc } from "firebase/firestore/lite";
import { db } from "../firebase/config";
import { getSuivi } from "./suivi";

const table = "States";

export const getState = async () => {
    try {
        const querySnapshot = await getDocs(collection(db, table))
        let datas = [];

        querySnapshot.forEach((doc) => {
            datas.push({
                ...doc.data(),
                id: doc.id
            })
        })

        return datas;
    } catch (error) {
        console.log(error);
        return [];
    }
}

export const findLabelById = (id, items) => {
    const item = items.find(item => item.id === id);
    return item ? item.trans : 'unknown'; // Retourne 'Non spécifié' si l'ID n'est pas trouvé
};

export const modificationStateId = async () => {
    const suivi = await getSuivi()

    suivi.datas.map(item => {
        if(item.state == "1"){
            item.state = "Yp3plBJpmlwAMds0M0cb"
            updateDoc(doc(db, "Suivi", item.idSuivi), item)
        }
        if(item.state == "2"){
            item.state = "ghgRUESBT9nfeRnDaQPy"
            updateDoc(doc(db, "Suivi", item.idSuivi), item)
        }
        if(item.state == "3"){
            item.state = "jYS2eEh25QnaWY9Peg44"
            updateDoc(doc(db, "Suivi", item.idSuivi), item)
        }
    })
}