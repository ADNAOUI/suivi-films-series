import {
  Timestamp,
  arrayRemove,
  arrayUnion,
  collection,
  doc,
  getDoc,
  getDocs,
  orderBy,
  setDoc,
  updateDoc
} from "firebase/firestore/lite";
import { db } from "../firebase/config";

const table = "Houses";

export const getHouses = async () => {
  let datas = [];

  try {
    const houses = await getDocs(collection(db, table), orderBy("create_at"));

    houses.forEach((house) => {
      datas.push(house.data());
    });

    return datas;
  } catch (error) {
    console.log("error", error);
  }
};

export const addHousesElementsByDoc = async (data, element, merge = true) => {
  data.create_at = Timestamp.fromDate(new Date()).toDate();

  switch (element) {
    case "Search":
      data.avis = data.avis === "no" ? false : true;
      break;

    default:
      break;
  }

  try {
    let array = [];
    const document = doc(db, table, element);
    const docSnapshot = await getDoc(document);

    if (merge) {
      if (docSnapshot.exists()) {
        // Si le document existe, obtenir les données existantes
        array = docSnapshot.data().datas || [];
      }
    } else {
      let existingData = docSnapshot.data()?.datas[0] ?? {};
      data = {
        ...existingData,
        ...data,
      };
    }

    array.push(data);

    await setDoc(document, { datas: array }, { merge: true });
  } catch (error) {
    console.log(error);
    throw new Error(error)
  }
};

export const getHousesElementsByDoc = async (element) => {
  const docRef = doc(db, table, element);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    return docSnap.data();
  } else {
    console.log("No such document!");
    throw new Error("No such document!")
  }
}

export const update = async (docId, data, del = false) => {
  const docRef = doc(db, table, docId);

  try {
    if (del) {
      await updateDoc(docRef, {
        datas: arrayRemove(data)
      });
      console.log("Produit supprimé avec succès !");
    } else {
      data.create_at = Timestamp.now();
      await updateDoc(docRef, {
        datas: arrayUnion(data)
      });
      console.log("Produit ajouté avec succès !");
    }

  } catch (error) {
    console.error("Erreur lors de la mise à jour du document :", error);
  }
};

export const remove = async (docId) => {
  const document = doc(db, table, docId);

  await updateDoc(document, {
    datas: []
  })
}