import { collection, doc, getDoc, getDocs, setDoc, Timestamp } from "firebase/firestore/lite";
import { db } from "../firebase/config";

const table = "Invoices";

export const getInvoices = async () => {
    try {
        const querySnapshot = await getDocs(collection(db, table))
        let datas = [];

        querySnapshot.forEach((doc) => {
            datas.push({
                ...doc.data(),
                id: doc.id
            })
        })

        return datas;
    } catch (error) {
        console.log(error);
        return [];
    }
}

export const addInvoice = async (data) => {
    data.create_at = Timestamp.now();

    try {
        const document = doc(db, table, data.name);

        // Récupérer les factures actuelles
        const docSnapshot = await getDoc(document);

        let invoices = [];

        if (docSnapshot.exists()) {
            // Si le document existe, obtenir les données existantes
            invoices = docSnapshot.data().data || [];
        }

        // Ajouter la nouvelle facture au tableau
        invoices.push(data);

        // Mettre à jour le document avec le nouveau tableau de factures
        await setDoc(document, { data: invoices }, { merge: true });
    } catch (error) {
        console.log(error);
        throw new Error(error)
    }
};