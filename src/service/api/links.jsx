import {
    doc,
    getDoc
} from "firebase/firestore/lite";
import { db } from "../firebase/config";

const name = "Links";

export const getLink = async (pathName = null) => {
    try {
        const path = location.pathname.replace(/^\/+/g, "")

        return (await getDoc(doc(db, name, pathName ?? path))).data();
    } catch (error) {
        console.log("error", error);
    }
};