import {
  getDownloadURL,
  getStorage,
  listAll,
  ref,
  uploadBytes,
} from "firebase/storage";
import Utils from "../../utils/Utils";

const storage = getStorage();

export const getBanner = async (image) => {
  try {
    if (!image)
      return false;

    const reference = ref(storage, `/${image}/banner.png`);
    return await getDownloadURL(reference);
  } catch (error) {
    console.error(`Error fetching image from Firebase Storage:${error}`);
    throw new Error(`Error fetching image from Firebase Storage:${error}`)
  }
};

export const getTravelImages = async () => {
  const listRef = ref(storage, "voyages/cities");

  return await listAll(listRef)
    .then((res) => {
      res.items.forEach(async (itemRef) => {
        return await getDownloadURL(itemRef);
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const uploadFiles = async ({ files, path, name, info }) => {
  let uploaded = [];
  let loading;

  try {
    const filesArray = Array.from(files);


    for (const [index, file] of filesArray.entries()) {
      const storageRef = ref(
        storage,
        `${path}/${name}/${index}-${name}-${info ? info + "-" : ""}${Utils.generateUniqueIDDocument()}`
      );

      const snapshot = await uploadBytes(storageRef, file);
      const url = await getDownloadURL(snapshot.ref);

      uploaded.push({
        original: url,
        originalAlt: `fichier pour ${name}`,
      });
    }

    loading = false;
    return { uploaded, loading };
  } catch (error) {
    console.error(`Error uploading file: ${error}`);
    throw new Error(`Error uploading file: ${error}`);
  }
}

export const uploadImages = async ({ images, path, name }) => {
  let uploaded = [];

  try {
    const imagesArray = Array.from(images);

    for (const [index, image] of imagesArray.entries()) {
      const storageRef = ref(
        storage,
        `${path}/${name}/${index}-${name}-${Utils.generateUniqueIDDocument()}`
      );

      const snapshot = await uploadBytes(storageRef, image);
      const url = await getDownloadURL(snapshot.ref);

      uploaded.push({
        original: url,
        originalWidth: 200,
        originalHeight: 200,
        originalAlt: `restaurant ${name}`,
      });
    }

    return { uploaded, loading: false };
  } catch (error) {
    console.error(`Error uploading file: ${error}`);
    throw new Error(`Error uploading file: ${error}`);
  }
};