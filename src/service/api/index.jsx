import { getCategories } from "./categories";
import { getBanner, uploadFiles, uploadImages } from "./files";
import { addHousesElementsByDoc, getHouses, getHousesElementsByDoc, remove, update } from "./houses";
import { addInvoice, getInvoices } from "./invoices";
import {
  addRestaurant,
  filterRestaurants,
  getRestaurants,
  getTypeRestaurants
} from "./restaurants";
import { findLabelById, getState, modificationStateId } from "./state";
import {
  addSuivi,
  countSuiviState,
  deleteSuivi,
  getSuivi,
  pageSizeSuivi,
  searchValue,
  updateSuivi
} from "./suivi";
import { getTravels, getVisitedCities } from "./travels";
import { getUsers } from "./users";

import { getLink } from "./links";
import { addTables, getTables } from "./tables";

export {
  addHousesElementsByDoc, addInvoice, addRestaurant, addSuivi, addTables, countSuiviState, deleteSuivi, filterRestaurants, findLabelById, getBanner, getCategories, getHouses, getHousesElementsByDoc, getInvoices, getLink, getRestaurants, getState,
  getSuivi, getTables, getTravels,
  getTypeRestaurants, getUsers, getVisitedCities, modificationStateId, pageSizeSuivi, remove, searchValue, update, updateSuivi, uploadFiles, uploadImages
};

