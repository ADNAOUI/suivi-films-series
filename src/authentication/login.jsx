import { EmailAuthProvider } from "firebase/auth";
import * as firebaseui from "firebaseui";
import "firebaseui/dist/firebaseui.css";
import { useEffect } from "react";
import { Header } from "../components";
import { auth } from "../service/firebase/config";

const uiConfig = {
  signInSuccessUrl: "/", // This is where should redirect if the sign in is successful.
  signInOptions: [
    // This array contains all the ways an user can authenticate in your application. For this example, is only by email.
    {
      provider: EmailAuthProvider.PROVIDER_ID,
      requireDisplayName: true,
      disableSignUp: {
        status: true,
      },
    },
  ],
};

export const SignInScreen = () => {
  useEffect(() => {
    const ui =
      firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(auth);

    ui.start("#firebaseui-auth-container", uiConfig);
    return () => {
      ui.delete();
    };
  }, []);

  return (
    <>
      <Header />
      <div id="firebaseui-auth-container" className="mt-5"></div>
    </>
  );
};
