import { signOut } from "firebase/auth";
import { useEffect } from "react";
import { useBannerText } from "../context/BannerTextContext";
import { auth } from "../service/firebase/config";
import { SignInScreen } from "./login";

export const Login = () => {
  const { setBannerText } = useBannerText();

  useEffect(() => {
    setBannerText("connexion");
  }, [setBannerText]);

  return <SignInScreen />;
};

export const Disconnect = () => {
  const logout = () => {
    signOut(auth);
  };

  return (
    <button type="button" onClick={logout}>
      logout
    </button>
  );
};
