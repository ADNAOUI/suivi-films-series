import { Card } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export const CustomCard = ({ card }) => {
    if (card) {
        return (
            <Card>
                <div className='card__link'>
                    <NavLink to={card.link} />
                </div>
                <Card.Img variant="top"
                    src={card.image}
                />
                <Card.Body>
                    <Card.Title>{card.name}</Card.Title>
                    <Card.Text>
                        {/* {card.text} */}
                    </Card.Text>
                </Card.Body>
            </Card>
        );
    }
}