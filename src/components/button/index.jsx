import { Button } from "react-bootstrap";

export const ButtonIcon = ({ classname, variant, title, icon, fnUpdate }) => {
  return (
    <Button className={classname} variant={variant} title={title} onClick={fnUpdate}>
      {icon}
    </Button>
  );
};

export const ButtonName = ({ title, fnUpdate }) => {
  return (
    <button className="button" type="button" onClick={fnUpdate}>
      {title}
    </button>
  );
};

export const ButtonImage = ({ id, text, upload, icon }) => {

  return (
    <>
      <label htmlFor={id} className="button-add-files">
        <span className="button-add-files__text">{text}</span>
        <span className="button-add-files__icon">
          <i className={`fa-solid fa-${icon}`}></i>
        </span>
      </label>
      <input
        id={id}
        name={id}
        type="file"
        onChange={upload}
        multiple
        accept="image/*"
        className="d-none"
      />
    </>
  )
}

export const ButtonLoader = ({ state = true, title, fnUpdate }) => {
  return (
    <Button
      type="submit"
      className="button loading background-tertiary-color"
      disabled={state}
    >
      {title}
      {state && <span className="loader"></span>}
    </Button>
  )
}