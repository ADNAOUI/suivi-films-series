import { useEffect, useState } from "react";
import { useBannerText } from "../../context/BannerTextContext";
import { getBanner } from "../../service/api";
import { useTranslation } from "react-i18next";
import Utils from "../../utils/Utils";

export const Banner = ({ text }) => {
  const { bannerText } = useBannerText();
  const [url, setUrl] = useState("");
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchDatas = async () => {
      const imageBanner = await getBanner(bannerText);
      setUrl(imageBanner);
      setLoading(false);
    };

    fetchDatas();
  }, [bannerText]);

  const arrayOfLists = Array.from(Array(10)).map((_, index) => (
    <li key={index}></li>
  ));

  if (!loading)
    return (
      <section className="banner">
        <div className="banner__background d-flex flex-column">
          <div
            className="banner__background__image"
            // style={backgroundImage: url}
            style={{
              backgroundImage: `url(${url})`,
              // width: "200px",
              // height: "200px"
            }}
          ></div>
          <div className="banner__background__title">
            <h1 variant="h1">{t(Utils.capitalizeFirstLetter(text))}</h1>
          </div>

          <div className="d-flex banner__background__animated">
            <ul className="circles">{arrayOfLists}</ul>
          </div>
        </div>
      </section>
    );
};
