export const CustomChekbox = ({ props, fn }) => {
    return (
        <div className="mb-2 custom-checkbox">
            <label htmlFor={props.id} className="d-flex gap-1">
                <input
                    type="checkbox"
                    id={props.id}
                    name={props.id}
                    value={props.id}
                    data-category={props.type}
                    onChange={fn}
                />
                {props.name}
            </label>
        </div>
    )
}