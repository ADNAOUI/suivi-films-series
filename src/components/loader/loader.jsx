import Image from "react-bootstrap/Image";

import Img from "../../assets/img/logo/logo-responsive.png";
export const Loader = () => {
  return (
    <div className="loader">
      <span className="loading"></span>
      <Image
        alt="loading logo responsive"
        src={Img}
        roundedCircle
        width={100}
      />
    </div>
  );
};
