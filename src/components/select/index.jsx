import { Form } from 'react-bootstrap';

export const CustomSelect = ({ options, defaultValue, value, onChange }) => {
    return (
        <Form.Select value={value} defaultValue={defaultValue} onChange={onChange}>
            <option>Choisir</option>
            {options.map((item, index) => (
                <option key={index} value={item.id}>{item.label ?? item.name}</option>
            ))}
        </Form.Select>
    );
}
