import { useState } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { NavLink } from "react-router-dom";
import Logo from "../../assets/img/logo/logo-color.png";

const Header = ({ links }) => {
  const [expanded, setExpanded] = useState(false);
  const { t } = useTranslation();

  const handleToggle = () => {
    setExpanded(!expanded);
  };

  const handleNavClick = () => {
    setExpanded(false);
  };

  const Linked = () => {
    if (links) {
      return (
        <>
          <Navbar.Toggle
            className="button-burger"
            aria-controls="basic-navbar-nav"
            onClick={handleToggle}
          />
          <Navbar.Collapse id="basic-navbar-nav" in={expanded}>
            <Nav className="me-auto">
              {links.map((item, index) => (
                <NavLink
                  key={index}
                  className="nav-link"
                  to={item.link}
                  onClick={handleNavClick}
                >
                  {t(item.name)}
                </NavLink>
              ))}
            </Nav>
          </Navbar.Collapse>
        </>
      );
    }
  };

  return (
    <header>
      <Navbar collapseOnSelect expand="lg" expanded={expanded}>
        <div className={`container-fluid ${!links ? "justify-content-center" : ""}`}>
          <div className="navbar-image">
            <img src={Logo} alt="react logo" width={!links ? 100 : 59} />
          </div>
          <Linked />
        </div>
      </Navbar>
    </header>
  );
};

export default Header;
