import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';

export const CustomBreadcrumb = () => {
    const location = useLocation();

    const paths = location.pathname.split("/").filter(Boolean);

    if (paths.length > 1)
        return (
            <>
                <Breadcrumb className='navbar'>
                    {paths.map((item, index, { length }) => (
                        <li key={index} className='breadcrumb-item navbar-nav d-flex flex-row align-items-center'>
                            {index + 1 === length
                                ? <span className='text-capitalize active'>{item}</span>
                                : <Link
                                    className="nav-link text-capitalize"
                                    to={item}
                                >
                                    {item}
                                </Link>
                            }
                        </li>
                    ))}
                </Breadcrumb>
            </>
        );
}