import { Table, Button } from "react-bootstrap";
import { deleteSuivi, findLabelById, updateSuivi } from "../../service/api";
import { useEffect, useState } from "react";
import { useToast, CustomSelect } from "../";

export const TableFirebase = ({ suivi, categories, states }) => {
  const [suiviData, setSuiviData] = useState(suivi);
  const [editingId, setEditingId] = useState(null);
  const [editingData, setEditingData] = useState({});

  const showToast = useToast();

  useEffect(() => {
    setSuiviData(suivi);
  }, [suivi]);

  const handleEdit = (id) => {
    setEditingId(id);
    const currentItem = suivi.find((item) => item.idSuivi === id);
    setEditingData(currentItem);
  };

  const handleChange = (field, value) => {
    // Met à jour editingData avec les nouvelles valeurs
    setEditingData((prev) => ({ ...prev, [field]: value }));
  };

  const handleDelete = (id) => {
    if (confirm("Êtes-vous sur de supprimer cette donnée ?")) {
      deleteSuivi(id);
      showToast("Donnée supprimée avec succès !", "success");

      setSuiviData((currentSuivi) =>
        currentSuivi.filter((item) => item.idSuivi !== id)
      );
    }
  };

  const saveChanges = () => {
    setEditingId(null);
    try {
      updateSuivi(editingData);
      setEditingData({});
      showToast("Données soumis avec succès !", "success");
    } catch (error) {
      showToast("Erreur lors de la soumission", "error");
      console.error(error);
    }
  };

  return (
    <Table bordered hover responsive className="table-firebase">
      <thead>
        <tr>
          <th>#</th>
          <th>Initiateur</th>
          <th>Titre</th>
          <th>Catégorie</th>
          <th>Etat d'avancement</th>
          <th>Note Farid (/10)</th>
          <th>Note Laurine (/10)</th>
          <th>Modifier</th>
          <th>Supprimer</th>
        </tr>
      </thead>
      <tbody>
        {suiviData &&
          suiviData.map((item, index) => (
            <tr
              data-id={item.idSuivi}
              key={item.idSuivi}
              className={`state ${findLabelById(item.state, states)}`}
            >
              <td>
                <button
                  className="button"
                  type="button"
                  onClick={() => navigator.clipboard.writeText(item.idSuivi)}
                >
                  {index + 1}
                </button>
              </td>
              <td>
                <textarea
                  defaultValue={item.initiator}
                  readOnly={editingId !== item.idSuivi}
                  onChange={(e) => handleChange("initiator", e.target.value)}
                ></textarea>
              </td>
              <td>
                <textarea
                  className="titre"
                  defaultValue={item.title}
                  readOnly={editingId !== item.idSuivi}
                  onChange={(e) => handleChange("title", e.target.value)}
                ></textarea>
              </td>
              <td>
                <CustomSelect
                  options={categories}
                  defaultValue={item.category}
                  onChange={(e) => handleChange("category", e.target.value)}
                />
              </td>
              <td>
                <CustomSelect
                  options={states}
                  defaultValue={item.state}
                  onChange={(e) => handleChange("state", e.target.value)}
                />
              </td>
              <td className="note">
                <input
                  type="number"
                  defaultValue={item.noteFarid}
                  readOnly={editingId !== item.idSuivi}
                  onChange={(e) => handleChange("noteFarid", e.target.value)}
                />
              </td>
              <td className="note">
                <input
                  type="number"
                  defaultValue={item.noteLaurine}
                  readOnly={editingId !== item.idSuivi}
                  onChange={(e) => handleChange("noteLaurine", e.target.value)}
                />
              </td>
              <td className="text-center">
                {editingId === item.idSuivi ? (
                  <Button
                    variant="success"
                    onClick={() => saveChanges(item.idSuivi)}
                  >
                    <i className="fa-solid fa-save"></i>
                  </Button>
                ) : (
                  <Button
                    variant="primary"
                    onClick={() => handleEdit(item.idSuivi)}
                  >
                    <i className="fa-solid fa-pen-to-square"></i>
                  </Button>
                )}
              </td>
              <td className="text-center">
                <Button
                  variant="danger"
                  onClick={() => handleDelete(item.idSuivi)}
                >
                  <i className="fa-solid fa-trash"></i>
                </Button>
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
  );
};
