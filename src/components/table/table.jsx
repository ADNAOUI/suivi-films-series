import {
  HeaderCellSort,
  SortToggleType,
  useSort,
} from "@table-library/react-table-library/sort";
import {
  Body,
  Cell,
  Header,
  HeaderCell,
  HeaderRow,
  Row,
  Table,
} from "@table-library/react-table-library/table";
import { useTheme } from "@table-library/react-table-library/theme";
import { useState } from "react";
import { Accordion, Form, InputGroup, ListGroup } from "react-bootstrap";
import { ButtonIcon, ButtonName, CustomSelect, useToast } from "../";
import { findLabelById, updateSuivi } from "../../service/api";

export const TableComponent = ({ suivi, categories, states, stats }) => {
  const [search, setSearch] = useState("");
  const [suiviData, setSuiviData] = useState(suivi);
  const [editingId, setEditingId] = useState(null);
  const [editingData, setEditingData] = useState({});
  const showToast = useToast();

  const theme = useTheme({
    Table: `
            --data-table-library_grid-template-columns: minmax(50px, 10%) minmax(70px, 10%) minmax(150px, 35%) minmax(100px, 10%) minmax(100px, 10%) minmax(50px, 5%) minmax(50px, 10%) minmax(50px, 5%) minmax(50px, 5%);
          `,
  });

  const { datas: nodes, loading } = suivi;
  let data = { nodes };

  const sort = useSort(
    data,
    {
      onChange: onSortChange,
    },
    {
      sortToggleType: SortToggleType.AlternateWithReset,
      sortFns: {
        INITIATOR: (array) =>
          array.sort((a, b) => a.initiator.localeCompare(b.initiator)),
        STATE: (array) => array.sort((a, b) => a.state.localeCompare(b.state)),
        CATEGORY: (array) => array.sort((a, b) => a.category - b.category),
        TITLE: (array) => array.sort((a, b) => a.title.localeCompare(b.title)),
        NOTEFARID: (array) => array.sort((a, b) => a.noteFarid - b.noteFarid),
        NOTELAURINE: (array) =>
          array.sort((a, b) => a.noteLaurine - b.noteLaurine),
      },
    }
  );

  function handleSearch(event) {
    setSearch(event.target.value);
  }

  function onSortChange(action, state) {
    console.log(action, state);
  }

  function handleEdit(id) {
    setEditingId(id);
    const currentItem = nodes.find((item) => item.idSuivi === id);
    setEditingData(currentItem);
  }

  function handleChange(field, value) {
    // Met à jour editingData avec les nouvelles valeurs
    setEditingData((prev) => ({
      ...prev,
      [field]: value,
    }));
  }

  function handleDelete(id) {
    if (confirm("Êtes-vous sur de supprimer cette donnée ?")) {
      deleteSuivi(id);
      showToast("Donnée supprimée avec succès !", "success");

      setSuiviData((currentSuivi) =>
        currentSuivi.filter((item) => item.idSuivi !== id)
      );
    }
  }

  const saveChanges = () => {
    setEditingId(null);
    try {
      updateSuivi(editingData);
      setEditingData({});
      showToast("Données soumis avec succès !", "success");
    } catch (error) {
      showToast("Erreur lors de la soumission", "error");
      console.error(error);
    }
  };

  data = {
    nodes: data.nodes.filter((item) =>
      item.title.toLowerCase().includes(search.toLowerCase())
    ),
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <div className="col-5">
        <Form.Label htmlFor="search">Recherche par titre:</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control
            id="search"
            value={search}
            onChange={handleSearch}
            aria-describedby="searchitems"
          />
        </InputGroup>
      </div>
      <div className="col-5">
        <Accordion>
          <Accordion.Item eventKey="0">
            <Accordion.Header>Stats</Accordion.Header>
            <Accordion.Body>
              <ListGroup>
                {stats.map((stat, index) => {
                  return (
                    <ListGroup.Item key={index}>
                      <span>{stat.name}</span>
                      <span>{stat.count}</span>
                    </ListGroup.Item>
                  );
                })}
              </ListGroup>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </div>

      <div className="col-12">
        <Table
          data={data}
          sort={sort}
          theme={theme}
          layout={{ custom: true, horizontalScroll: true }}
          className="table table-firebase"
        >
          {(tableList) => (
            <>
              <Header>
                <HeaderRow>
                  <HeaderCell>#</HeaderCell>
                  <HeaderCellSort sortKey="INITIATOR">
                    Initiateur
                  </HeaderCellSort>
                  <HeaderCellSort sortKey="TITLE">Titre</HeaderCellSort>
                  <HeaderCellSort sortKey="CATEGORY">Catégorie</HeaderCellSort>
                  <HeaderCellSort sortKey="STATE">
                    Etat d'avancement
                  </HeaderCellSort>
                  <HeaderCellSort sortKey="NOTEFARID">
                    Note Farid (/10)
                  </HeaderCellSort>
                  <HeaderCellSort sortKey="NOTELAURINE">
                    Note Laurine (/10)
                  </HeaderCellSort>
                  <HeaderCell>Modifier</HeaderCell>
                  <HeaderCell>Supprimer</HeaderCell>
                </HeaderRow>
              </Header>
              <Body>
                {tableList.map((item, index) => (
                  <Row
                    key={item.idSuivi}
                    item={item}
                    className={`state ${findLabelById(item.state, states)}`}
                  >
                    <Cell>
                      <ButtonName
                        title={index + 1}
                        fnUpdate={() =>
                          navigator.clipboard.writeText(item.idSuivi)
                        }
                      />
                    </Cell>
                    <Cell>
                      <textarea
                        type="text"
                        defaultValue={item.initiator}
                        readOnly={editingId !== item.idSuivi}
                        onChange={(e) =>
                          handleChange("initiator", e.target.value)
                        }
                      />
                    </Cell>
                    <Cell>
                      <textarea
                        className="titre"
                        defaultValue={item.title}
                        readOnly={editingId !== item.idSuivi}
                        onChange={(e) => handleChange("title", e.target.value)}
                      />
                    </Cell>
                    <Cell>
                      <CustomSelect
                        options={categories}
                        defaultValue={item.category}
                        onChange={(e) =>
                          handleChange("category", e.target.value)
                        }
                      />
                    </Cell>
                    <Cell>
                      <CustomSelect
                        options={states}
                        defaultValue={item.state}
                        onChange={(e) => handleChange("state", e.target.value)}
                      />
                    </Cell>
                    <Cell>
                      <input
                        type="number"
                        defaultValue={item.noteFarid}
                        readOnly={editingId !== item.idSuivi}
                        onChange={(e) =>
                          handleChange("noteFarid", e.target.value)
                        }
                      />
                    </Cell>
                    <Cell>
                      <input
                        type="number"
                        defaultValue={item.noteLaurine}
                        readOnly={editingId !== item.idSuivi}
                        onChange={(e) =>
                          handleChange("noteLaurine", e.target.value)
                        }
                      />
                    </Cell>
                    <Cell>
                      {editingId === item.idSuivi ? (
                        <ButtonIcon
                          variant="success"
                          title="sauvegarder"
                          icon={<i className="fa-solid fa-save"></i>}
                          fnUpdate={() => saveChanges(item.idSuivi)}
                        />
                      ) : (
                        <ButtonIcon
                          title="modifier"
                          icon={<i className="fa-solid fa-pen-to-square"></i>}
                          fnUpdate={() => handleEdit(item.idSuivi)}
                        />
                      )}
                    </Cell>
                    <Cell>
                      <ButtonIcon
                        variant="danger"
                        title="supprimer"
                        icon={<i className="fa-solid fa-trash"></i>}
                        fnUpdate={() => handleDelete(item.idSuivi)}
                      />
                    </Cell>
                  </Row>
                ))}
              </Body>
            </>
          )}
        </Table>
      </div>
    </>
  );
};
