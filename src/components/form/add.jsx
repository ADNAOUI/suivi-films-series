import { useState } from "react";
import { Accordion, Col, Form, Image, Row } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ButtonImage, ButtonLoader, CustomSelect, useToast } from "../";
import { uploadFiles, uploadImages } from "../../service/api";

export const Add = ({ form, files = false, images = false, onSubmit, accept }) => {
  const [validated, setValidated] = useState(false);
  const [data, setDatas] = useState();
  const [file, setFile] = useState();
  const [image, setImage] = useState();
  const [thumbnails, setThumbnails] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [loading, setLoading] = useState(false);
  const [formKey, setFormKey] = useState(0); // Ajout d'une clé pour le formulaire
  const showToast = useToast();
  const pathname = location.pathname.replace(/\//g, "");

  function upload(e, type) {
    const files = e.target.files;

    switch (type) {
      case "image":
        const array = [];

        for (const file of files) {
          array.push({
            urlImage: URL.createObjectURL(file),
            file,
          });
        }
        setThumbnails(array);
        setImage(files);
        break;

      case "file":
        setFile(files);
        break;

      default:
        throw new Error("type non existant");
    }
  }

  const handleSubmit = async (event) => {
    const evt = event.currentTarget;
    setLoading(true);
    event.preventDefault();

    if (evt.checkValidity() === false) {
      event.stopPropagation();
      setLoading(false);
    } else {
      try {
        if (images && image) {
          const imageUrl = await uploadImages({
            images: image,
            path: pathname,
            name: data.name,
          });

          setLoading(imageUrl.loading);
          data.images = imageUrl.uploaded;
        }

        if (files && file) {
          const fileUrl = await uploadFiles({
            files: file,
            path: pathname,
            name: data.name,
            info: data.info,
          });

          setLoading(fileUrl.loading);
          data.files = fileUrl.uploaded;
        }

        if (!files && !images) setLoading(false);

        await onSubmit(data);
        showToast("Données soumises avec succès !", "success");
      } catch (error) {
        showToast(`Une erreur est survenue : ${error}`, "error");
        setLoading(false);
      }
    }

    setValidated(true);
    // reset form
    resetForm(event)
  };

  function resetForm(event) {
    event.target.reset();

    // Forcer le re-render du formulaire en changeant la clé
    setFormKey((prevKey) => prevKey + 1);
    setStartDate(new Date()); // Réinitialiser la date
    setDatas({}); // Réinitialiser les données
    setThumbnails([]); // Réinitialiser les miniatures
    setImage(null); // Réinitialiser les images
    setFile(null); // Réinitialiser les fichiers
  }

  const handleChange = (name, value) => {
    setDatas((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  function renderFormGroup(item, index) {
    switch (form[item].type) {
      case "select":
        return (
          <Col xs={12} md={form[item].as}>
            <CustomSelect
              options={form[item].options || []}
              onChange={(e) => handleChange(item, e.target.value)}
            />
          </Col>
        );

      case "date":
        return (
          <Col xs={12} md={form[item].as}>
            <DatePicker
              selected={startDate}
              onChange={(date) => {
                handleChange(item, date);
                setStartDate(date);
              }}
              timeInputLabel="Time:"
              dateFormat="dd/MM/yyyy h:mm aa"
              showTimeInput
              value={startDate}
            />
          </Col>
        );

      case "radio":
        return form[item].radioInput.map((check, index) => {
          const key = Object.keys(check)[0];
          const value = check[key];

          return (
            <Form.Check
              key={index}
              name={`group-${form[item].trans}`}
              type={form[item].type}
              id={`default-${key}`}
              label={`${value.trans}`}
              value={key}
              onChange={(e) => {
                handleChange(item, e.target.value);
              }}
            />
          );
        });

      default:
        return (
          <Form.Control
            // required={form[item].required}
            type={form[item].type}
            as={form[item].as}
            rows={form[item].rows}
            value={form[item].name}
            defaultValue={form[item].defaultValue}
            onChange={(e) => handleChange(item, e.target.value)}
          />
        );
    }
  }

  function removeImage(e) {
    let found = thumbnails.filter(
      (element) => element.urlImage !== e.target.dataset.urlImage
    );
    setThumbnails(found);

    let arr = [];

    found.map((item) => {
      return arr.push(item.file);
    });
    setImage(arr);
  }

  return (
    <Form
      key={formKey} // Utilisation de la clé ici pour forcer le re-render
      className="mb-5"
      noValidate
      validated={validated}
      onSubmit={handleSubmit}
    >
      <Row className="mb-3">
        <div className="col-12">
          <Accordion className="restaurant">
            <Accordion.Item eventKey="0">
              <Accordion.Header>Ajouter</Accordion.Header>
              <Accordion.Body>
                {images && (
                  <Row className="mb-3">
                    <Col>
                      <div className="width-fit-content mb-3">
                        <ButtonImage
                          id="add-image"
                          icon="cloud-arrow-up"
                          upload={(e) => upload(e, "image")}
                          text="Ajouter une image"
                          accept={accept}
                        />
                      </div>
                      <ul className="list-unstyled d-flex gap-2 flex-wrap">
                        {thumbnails &&
                          thumbnails.map((item, index) => {
                            return (
                              <li
                                onClick={removeImage}
                                data-url-image={item.urlImage}
                                key={index}
                                className="image-remove"
                              >
                                <Image
                                  src={item.urlImage}
                                  rounded
                                  width="100px"
                                  height="100px"
                                />
                              </li>
                            );
                          })}
                      </ul>
                    </Col>
                  </Row>
                )}

                {files && (
                  <>
                    <label htmlFor="add-image">Ajouter un fichier</label>
                    <input
                      name="add-image"
                      type="file"
                      onChange={(e) => upload(e, "file")}
                      multiple
                      accept={accept}
                      required
                    />
                  </>
                )}

                <Row className="mb-3">
                  {Object.keys(form).map((item, index) => {
                    return (
                      <Form.Group
                        key={index}
                        as={Col}
                        md={form[item].md}
                        controlId={form[item].name}
                        className="mb-2"
                      >
                        <Form.Label>{form[item].trans}</Form.Label>
                        {renderFormGroup(item, index)}
                      </Form.Group>
                    );
                  })}
                </Row>
                <ButtonLoader state={loading} title="Ajouter" />
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
      </Row>
    </Form>
  );
};
