import { Form } from "react-bootstrap"

export const Search = ({ title, id, fnOnChange }) => {

    return (
        <Form>
            <Form.Group className="mb-3" controlId={id}>
                <Form.Label>{title}</Form.Label>
                <Form.Control type="search" onChange={fnOnChange} />
            </Form.Group>
        </Form>
    )
}