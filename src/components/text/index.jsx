export const Title = ({ text }) => {
    return (
        <h2 className="mb-3"><span className="subtitle">{text}</span></h2>
    )
}