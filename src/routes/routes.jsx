import { Container } from "react-bootstrap";
import { useRoutes } from "react-router";
import { CustomBreadcrumb } from "../components";
import {
  City,
  Cooking,
  Country,
  Environment,
  Food,
  Game,
  Home,
  Houses,
  Invoices,
  NotFound,
  Restaurants,
  Search,
  Travel,
  Wanted
} from "../pages";
import { Debts } from "../pages/Houses";

const routesConfig = [
  {
    path: "/",
    element: <Home />,
    private: true,
  },
  {
    path: "travels",
    children: [
      { path: "", element: <Travel />, private: true },
      {
        path: ":idCountry",
        children: [
          { path: "", element: <Country /> },
          {
            path: ":idCity",
            element: <City />,
          },
        ],
      },
    ],
  },
  {
    path: "cookings",
    element: <Cooking />,
    private: true,
  },
  {
    path: "restaurants",
    element: <Restaurants />,
    private: false,
  },
  {
    path: "houses",
    children: [
      {
        path: "",
        element: <Houses />
      },
      {
        path: "search",
        element: <Search />
      },
      {
        path: "environment",
        element: <Environment />
      },
      {
        path: "food",
        element: <Food />
      },
      {
        path: "wanted",
        element: <Wanted />
      },
      {
        path: "debts",
        element: <Debts />
      },
      {
        path: "game",
        element: <Game />
      },
    ],
  },
  {
    path: "invoices",
    element: <Invoices />,
    private: false,
  },
  {
    path: "/*",
    element: <NotFound />,
    private: false,
  },
];

const RouteAsObj = () => {
  const routes = useRoutes(routesConfig);

  return (
    <Container className="min-vh-50">
      <CustomBreadcrumb />
      {routes}
    </Container>
  );
};

export default RouteAsObj;
