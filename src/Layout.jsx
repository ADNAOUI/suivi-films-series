import { useEffect, useState } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { Login } from "./authentication";
import { Banner, Header, Toaster } from "./components";
import { useUser } from "./context/AuthContext";
import { useBannerText } from "./context/BannerTextContext";
import RouteAsObj from "./routes/routes";
import { getLink } from "./service/api";
import i18n from "./translate/translate";

const Layout = () => {
  const navigate = useNavigate();
  const { user, loading } = useUser();
  const { bannerText } = useBannerText();
  const [links, setLinks] = useState();

  const fetchDatas = async () => {
    const linksData = await getLink("header");

    setLinks(linksData.datas)
  };

  useEffect(() => {
    i18n.changeLanguage();
    
    if (!loading) {
      if (!user) {
        navigate("/login");
      } else {
        fetchDatas();
      }
    }

  }, [user, loading]);

  if (!user) {
    return (
      <Routes>
        <Route path="/login" element={<Login />} />
      </Routes>
    );
  }
  else {
    return (
      <>
        <Header links={links} />
        <Banner text={bannerText} image={bannerText} />
        <Routes>
          <Route path="/*" element={<RouteAsObj />} />:
        </Routes>
        <Toaster />
      </>
    );
  }
};

export default Layout;
