import React, { createContext, useContext, useState } from "react";

// Création du contexte
const BannerTextContext = createContext();

// Création du fournisseur de contexte
export const BannerTextProvider = ({ children }) => {
  const [bannerText, setBannerText] = useState(); // Valeur par défaut

  return (
    <BannerTextContext.Provider value={{ bannerText, setBannerText }}>
      {children}
    </BannerTextContext.Provider>
  );
};

export const useBannerText = () => useContext(BannerTextContext);
